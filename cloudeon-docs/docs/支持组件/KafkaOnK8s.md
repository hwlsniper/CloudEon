# Kafka on  K8s
安装Kafka服务
![图片.png](../images/kafka-1.png)

分配角色实例到指定节点安装
![图片.png](../images/kafka-2.png)

修改初始化配置，一般不用调整
![图片.png](../images/kafka-3.png)

等待安装成功
![图片.png](../images/kafka-4.png)

